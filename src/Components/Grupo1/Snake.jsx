import Snake from 'snake-game-react';

export default function Vibora(props) {
    return (
      <div>
        <h1 style={{textAlign: 'center'}}>Game Snake</h1>
        <p style={{textAlign: 'center'}}>Pedro Deiby Sejas Churco</p>
          <p style={{textAlign: 'center'}}>
            <b>NOTA: </b>hacer click en el tablero, antes de presionar las flechas del teclado
          </p>
        <Snake 
          color1={props.colorVib}
          color2={props.colorCom}
          backgroundColor={props.colorFond}
          />  
      </div>
    );
  }