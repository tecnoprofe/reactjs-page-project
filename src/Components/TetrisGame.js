import React, { useState } from 'react'
import Tetris from 'react-simple-tetris';
import './Grupo1/Tetris.css';

class TetrisGame extends React.Component {
  constructor (props) {
    super(props)
  }

  render() {
    return (
      <div className='Container'>
        <div className='Jugador-info'>
          <h1>Tetris</h1>
          <h2>Jugador: {this.props.nombre}</h2>
        </div>

        <div className='TetrisGame'>
        <Tetris
            keyboardControls={{
              // Default values shown here. These will be used if no
              // `keyboardControls` prop is provided.
              down: "MOVE_DOWN",
              left: "MOVE_LEFT",
              right: "MOVE_RIGHT",
              space: "HARD_DROP",
              z: "FLIP_COUNTERCLOCKWISE",
              x: "FLIP_CLOCKWISE",
              up: "FLIP_CLOCKWISE",
              p: "TOGGLE_PAUSE",
              c: "HOLD",
              shift: "HOLD",
            }}
          >
            {({
              HeldPiece,
              Gameboard,
              points,
              linesCleared,
              PieceQueue,
              state,
              controller,
            }) => (
              <div>
                <div>
                  <p>Puntos: {points}</p>
                </div>
                <Gameboard />
                {state === "LOST" && (
                  <div>
                    <h2>Game Over</h2>
                    <button onClick={controller.restart}>
                      New game
                    </button>
                  </div>
                )}
              </div>
            )}
          </Tetris>
        </div>

        <div className='Instrucciones'>
        <h2>Instrucciones</h2>
        <p><strong>z:</strong> Girar <br />
        <strong>Espacio:</strong> Soltar <br />
        <strong>Mover Izquierda:</strong> Flecha Izquierda <br />
        <strong>Mover Derecha:</strong> Flecha derecha</p>
        </div>
          
      </div>

    )
  }
}

export default TetrisGame