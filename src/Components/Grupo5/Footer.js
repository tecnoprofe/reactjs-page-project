import React from "react";
import styled from "styled-components";
import "./Footer.css";

function Footer() {
  return (
    <FooterContainer className="main-footer">
      <div className="footer-middle">
        <div className="containerf">
          <div className="row">
            {/* Column 1 */}
            <div className="col-md-3 col-sm-6">
              <h4 className="h4f">Proyecto Final</h4>
              <ul className="list-unstyled">
                <li className="liFooter">
                  Elaborado por Jose andres Galarza Chavez estudiante de la
                  carrera de Ingeniería de
                </li>
                <li className="liFooter">sistemas en la universidad Privada Domingo</li>
                <li className="liFooter">Savio actualmente cursando la materia de</li>
                <li className="liFooter">Tecnología Web 1</li>
              </ul>
            </div>
            {/* Column 2 */}
            <div className="col-md-3 col-sm-6">
              <h4 className="h4f">Lenguajes utilizados</h4>
              <ul className="list-unstyled">
                <li className="liFooter">
                  <a href="https://www.javascript.com">JavaScript</a>
                </li>
                <li className="liFooter">
                  <a href="https://lenguajehtml.com">HTML</a>
                </li>
                <li className="liFooter">
                  <a href="https://lenguajecss.com">CSS</a>
                </li>
              </ul>
            </div>
            {/* Column 3 */}
            <div className="col-md-3 col-sm-6">
              <h4 className="h4f">Librerias utlizadas</h4>
              <ul className="list-unstyled">
                <li className="liFooter">
                  <a href="https://react-bootstrap.github.io/getting-started/introduction">
                    React-bootstrap
                  </a>
                </li>
                <li className="liFooter">
                  <a href="https://styled-components.com/docs/basics#installation">
                    styled-components
                  </a>
                </li>
              </ul>
            </div>
            {/* Column 4 */}
            <div className="col-md-3 col-sm-6">
              <h4 className="h4f">Información adicional</h4>
              <ul className="list-unstyled">
                <li className="liFooter">Telefono: 333-444-666</li>
                <li className="liFooter">gitlab: andresgc</li>
                <li className="liFooter">correo: example@gmail.com</li>
                <li className="liFooter">Codigo del juego TicTacToc:</li>
                <li className="liFooter">
                  <a href="https://codepen.io/gaearon/pen/gWWZgR?editors=0010">
                    TicTacToe
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        {/* Footer Bottom */}
        <div className="footer-bottom">
          <p className="text-xs-center">
            &copy;{new Date().getFullYear()} City Guide App - All Rights
            Reserved
          </p>
        </div>
      </div>
    </FooterContainer>
  );
}

export default Footer;

const FooterContainer = styled.footer`
  .footer-middle {
    background: var(--mainDark);
    padding-top: 3rem;
    color: var(--mainWhite);
  }

  .footer-bottom {
    padding-top: 3rem;
    padding-bottom: 2rem;
  }

  ul li a {
    color: var(--mainGrey);
  }

  ul li a:hover {
    color: var(--mainLightGrey);
  }
`;
