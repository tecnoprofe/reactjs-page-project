import React, { Component } from "react";
import "./Cgrupo5.css";
import { Navbar, Nav } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "../../pages/Home";
import Juego from "../../pages/Game";
import Music from "../../pages/Music";
import Grupo1 from "../../pages/Grupo1";
import Grupo2 from "../../pages/Grupo2";
import Grupo3 from "../../pages/Grupo3";
import Grupo4 from "../../pages/Grupo4";
import Grupo5 from "../../pages/Grupo5";
import { BrowserRouter } from "react-router-dom/cjs/react-router-dom.min";

export default class NavbarComp extends Component {
  render() {
    return (
      <Router>
        <div className="Cgrupo5">
          <Navbar className="navBg2" variant="dark" expand="lg">
            <Navbar.Brand href="./Home" className="NB2">UPDS-PROYECTOS</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav
                className="mr-auto my-2 my-lg-0"
                style={{ maxHeight: "250px" }}
                responsive-navbar-nav
              >
                <Nav.Link as={Link} to="/inicio">
                  Inicio
                </Nav.Link>
                <Nav.Link as={Link} to="/game">
                  Juego
                </Nav.Link>
                <Nav.Link as={Link} to="/music">
                  Musica
                </Nav.Link>
                <Nav.Link as={Link} to="/grupo1">
                  Grupo1
                </Nav.Link>
                <Nav.Link as={Link} to="/grupo2">
                  Grupo2
                </Nav.Link>
                <Nav.Link as={Link} to="/grupo3">
                  Grupo3
                </Nav.Link>
                <Nav.Link as={Link} to="/grupo4">
                  Grupo4
                </Nav.Link>
                <Nav.Link as={Link} to="/grupo5">
                  Grupo5
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <div>
          <Switch>
            <Route path="/game">
              <Juego />
            </Route>
            <Route path="/music">
              <Music list={ [{title: 'LLorando se fue', artist: 'Kjarkas'},
                          {title: 'La bomba', artist: 'Azul Azul'},
                          {title: 'Enamorarte de alguien mas', artist: 'Morat'} ]}/>
            </Route>
            <Route path="/grupo1">
              <Grupo1 />
            </Route>
            <Route path="/grupo2">
              <Grupo2 />
            </Route>
            <Route path="/grupo3">
              <Grupo3 />
            </Route>
            <Route path="/grupo4">
            <BrowserRouter>
            <Grupo4 />
            </BrowserRouter>
            
            </Route>
             
            <Route path="/grupo5">
              <Grupo5 />
            </Route>
            <Route path="/">
                <Home nombre="Página Inicio" logotipo="https://www.upds.edu.bo/wp-content/uploads/2020/07/logotipo-upds-azul.png"
                />
                <Home nombre="ddd okkk" logotipo="https://cdn.icon-icons.com/icons2/2407/PNG/512/gitlab_icon_146171.png"/>
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}
