import React, { Component } from "react";
import Carousel from "./Carousel";
import Footer from "./Footer";
import { Link } from "react-router-dom";
import styled from "styled-components";
import "./Cgrupo5.css";

export default class Home extends Component {
  render() {
    return (
      <div className="Cgrupo5">
        <h1 style={{ fontSize: 60, marginLeft: "100px", marginTop:'20px', textAlign: "left" }}>
          Home
        </h1>
        <hr />
        <Carousel className="carousel" />
        <h1 style={{ paddingBottom: "50px", paddingTop: "50px" }}>
          Este Proyecto se realizo
          <br />
          con los siguientes leguanjes de programación
        </h1>
        <div style={{width:"100%", display:"flex"}}>
          <div style={{width: "50%"}}>
            <div>
              <h2>React</h2>
              <hr />
              <p style={{ fontSize: 22, marginBottom: "30px" }}>
                React es una biblioteca Javascript de código abierto diseñada para
                <br />
                crear interfaces de usuario con el objetivo de facilitar el
                <br />
                desarrollo de aplicaciones en una sola página.
              </p>
            </div>
            <div style={{paddingTop:'190px'}}>
              <h2>HTML</h2>
              <hr />
              <p style={{ fontSize: 22, marginBottom: "30px" }}>
                HTML, siglas en inglés de HyperText Markup Language, hace
                referencia
                <br />
                al lenguaje de marcado para la elaboración de páginas web.
              </p>
            </div>
          </div>
          <div  style={{width: "50%"}}>
            <div style={{paddingTop:'190px'}}>
              <h2>JavaScript</h2>
              <hr />
              <p style={{ fontSize: 22, marginBottom: "30px" }}>
                JavaScript es un lenguaje de programación interpretado, dialecto
                del
                <br />
                estándar ECMAScript. Se define como orientado a objetos,​ basado
                en
                <br />
                prototipos, imperativo, débilmente tipado y dinámico.
              </p>
            </div>
            <div style={{paddingTop:'190px'}}>
              <h2>CSS</h2>
              <hr />
              <p style={{ fontSize: 22, marginBottom: "30px" }}>
                CSS, en español «Hojas de estilo en cascada, es un lenguaje de
                <br />
                diseño gráfico para definir y crear la presentación de un
                documento
                <br />
                estructurado escrito en un lenguaje de marcado.​
              </p>
            </div>
          </div>
        </div>
        <hr />
        <DuoContainer className="main-duo">
          <div className="duo-middle">
            <div className="containerd">
              <div className="row">
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/home">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/85a54b84f53cfe25fc8d9ff17e5a60f3.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/gamet">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/b30ce49d39c308bb3031647f31f6c3e9.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/about">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/25f750f861cdffc01551d728938be59b.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </DuoContainer>
        <Footer />
      </div>
    );
  }
}

const DuoContainer = styled.footer`
  .duo-middle {
    background: white;
  }
`;
