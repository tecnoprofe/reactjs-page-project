import React, { Component } from "react";
import "./Cgrupo5.css";
import { Navbar, Nav } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./Home";
import Gamet from './Gamet'
import About from "./About";

export default class NavbarComp extends Component {
  render() {
    return (
      <Router>
        <div className="Cgrupo5">
          <Navbar className="navBg" variant="dark" expand="lg" >
            <Navbar.Brand href="./grupo5" className="NB1">Grupo 5 - JoseAndresGalarzaChavez</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav
                className="mr-auto my-2 my-lg-0 ms-auto"
                style={{ maxHeight: "110px"}}
                responsive-navbar-nav
              >
                <Nav.Link as={Link} to="/home">
                  Home
                </Nav.Link>
                <Nav.Link as={Link} to="/gamet">
                  Game
                </Nav.Link>
                <Nav.Link as={Link} to="/about">
                  About
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <div>
          <Switch>
            <Route path="/gamet">
              <Gamet />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}
