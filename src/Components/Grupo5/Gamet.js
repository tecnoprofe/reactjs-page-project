import React, { Component } from "react";
import Game from "./Tictactoe";
import Footer from "./Footer";
import { Link } from "react-router-dom";
import styled from "styled-components";
import "./Cgrupo5.css";

export default class About extends Component {
  render() {
    return (
      <div className="Cgrupo5">
        <h1 style={{fontSize: 60, marginLeft:'100px', marginTop:'20px', textAlign:"left"}}>Game</h1>
        <hr />
        <h2 className="tictactoc-titulo" style={{color:'white'}}>TicTacToe</h2>
        <Game />
        <h2 style={{marginTop:'30px'}}>Juego TicTacToe</h2>
        <p style={{ fontSize: 22, marginBottom:'30px', marginTop:'30px'}}>
          El tres en línea, también conocido como ceros y cruces, tres en raya,<br/>
          cerito cruz, michi, triqui, cuadritos, juego del gato, gato, tatetí,<br/>
          totito, triqui traka, equis cero, tic-tac-toe o la vieja es un juego<br/>
          de lápiz y papel entre dos jugadores: O y X, que marcan los espacios<br/>
          de un tablero de 3×3 alternadamente.
        </p>
        <hr/>
        <DuoContainer className="main-duo">
          <div className="duo-middle">
            <div className="containerd">
              <div className="row">
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/home">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/85a54b84f53cfe25fc8d9ff17e5a60f3.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/gamet">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/b30ce49d39c308bb3031647f31f6c3e9.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="col-xs-6 col-md-4">
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/about">
                        <img
                          src="https://d35aaqx5ub95lt.cloudfront.net/vendor/25f750f861cdffc01551d728938be59b.svg"
                          alt=""
                          width="70"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </DuoContainer>
        <Footer />
      </div>
    );
  }
}

const DuoContainer = styled.footer`
  .duo-middle {
    background: white;
  }
`;
