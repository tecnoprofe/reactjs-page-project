import React, { Component } from "react";
import "./Cgrupo5.css";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Footer from "./Footer";
import "./Cgrupo5.css";

export default class Contact extends Component {
  render() {
    return (
      <div className="Cgrupo5">
        <h1 style={{ fontSize: 60, marginLeft: "100px", marginTop:'20px', textAlign: "left" }}>
          About
        </h1>
        <hr />
        <div style={{ width: "100%", display: "flex" }}>
          <div
            style={{
              textAlign: "left",
              width: "50%",
            }}
          >
            <img
              style={{ width: "100%", marginBottom: "100px" }}
              src="https://www.wallpaperuse.com/wallp/49-495663_m.jpg"
              alt=""
            ></img>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              Jose Andres es una artista Boliviano nacido en Santa Cruz conocida
              por su trabajo en la web. Después de asistir a la escuela de arte
              en SC, el comenzó a trabajar como diseñador y desarrollador de CSS
              en software consultante.
            </p>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              Siempre molesta por estar encajonado, ha pasado su carrera
              persiguiendo esfuerzos interdisciplinarios y ocupando roles no
              convencionales, pero aun asi se sigue esforzando por dar lo mejor
              de si.
            </p>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              En la década de 2020, comenzó a experimentar con la web como medio
              creativo. Utilizando tecnologías web establecidas de nuevas
              maneras. Su la actualización anual de la cartera gana popularidad
              cada año como ejemplo de sorprendente diseño receptivo y su
              proyecto de dibujo, A Single Div, demuestra el poder artístico de
              CSS.
            </p>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              Con el objetivo de crear rincones de nicho especiales de la web,
              ella ha construido una colección de proyectos dedicados a códigos
              de aeropuerto, edad brechas en la película, películas falsas en
              películas, banderas estatales, Arizona y programas de televisión
              The Good Place, Schitt's Creek y Top Chef.
            </p>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              Su trabajo ha sido presentado por CNN, NPR, Vox, la revista NET y
              Semanal de entretenimiento, tambien fue alguien reconocido por su
              trabajo en Frontend.
            </p>
            <p style={{ fontSize: 22, marginBottom: "100px", textAlign:"center" }}>
              Actualmente trabaja con la gente creativa de Netflix y vive en
              Bolivia, SC con su esposa Clay y sus dos perros, Boomer y Helo.
              Tambien sigue estudiando y practicando con diversos proyectos
              independientes enfocados en el diseño, el arte y todo lo
              relacionado con la creatividad.
            </p>
          </div>
          <div style={{ width: "50%" }}>
            <DuoContainer className="main-duo">
              <div className="duo-middle">
                <div className="containerd">
                  <div className="row">
                    <div className="col-xs- col-md-">
                      <ul className="list-unstyled">
                        <li>
                          <Link to="/home">
                            <img
                              src="https://d35aaqx5ub95lt.cloudfront.net/vendor/85a54b84f53cfe25fc8d9ff17e5a60f3.svg"
                              alt=""
                              width="100%"
                            />
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="col-xs- col-md-">
                      <ul className="list-unstyled">
                        <li>
                          <Link to="/gamet">
                            <img
                              src="https://d35aaqx5ub95lt.cloudfront.net/vendor/b30ce49d39c308bb3031647f31f6c3e9.svg"
                              alt=""
                              width="100%"
                            />
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="col-xs- col-md-">
                      <ul className="list-unstyled">
                        <li>
                          <Link to="/about">
                            <img
                              src="https://d35aaqx5ub95lt.cloudfront.net/vendor/25f750f861cdffc01551d728938be59b.svg"
                              alt=""
                              width="100%"
                            />
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </DuoContainer>
          </div>
        </div>
        <hr />
        <Footer />
      </div>
    );
  }
}

const DuoContainer = styled.footer`
  .duo-middle {
    background: white;
  }
`;
