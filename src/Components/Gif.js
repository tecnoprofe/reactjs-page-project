import React from "react";
import { Player, Controls } from '@lottiefiles/react-lottie-player';
class Gif extends React.Component{
    constructor (props) {
        super(props)
        this.player = React.createRef();
  }

  doSomething() {
    this.player.current.play(); 
  }

  render() {
    return (
      <Player
        onEvent={event => {
          if (event === 'load') this.doSomething(); 
        }}
        ref={this.player}
        autoplay={false}
        loop={true}
        controls={true}
        src="https://assets3.lottiefiles.com/packages/lf20_XZ3pkn.json"
        style={{ height: '300px', width: '300px' }}
      ></Player>
    );
        
    }
}
export default Gif;