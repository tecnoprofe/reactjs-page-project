import './estilo.css';
import ReactHowler from 'react-howler';
import React from 'react';
import Button from './Button';
import musica from './musica.jpg';
import ReactPlayer from 'react-player';

class App extends React.Component {
  
  constructor (props){
    super(props)

    this.state={
       playing: false
    }
    this.handlePlay = this.handlePlay.bind(this)
    this.handlePause = this.handlePause.bind(this)
  }

  handlePlay(){
    this.setState({
      playing: true
    })
  }

  handlePause(){
    this.setState({
      playing: false
    })
  }

  render (){
    return ( 
      <div class="Cancion" >
      <div class="titulo">
   
      <h3>1.Quevedo: Bzrp Music Sessions, Vol. 52 </h3>
      </div>
          <div align="center"> 
            
            <ReactHowler
              src='https://mp3teca.app/-/mp3/128/Bizarap%20ft%20Quevedo%20%E2%80%93%20Quevedo%20Bzrp%20Music%20Sessions%20Vol.%2052.mp3' 
              playing={this.state.playing}
              />
            <img src="" alt="" srcset="" />
            <Button onClick={this.handlePlay}>Play</Button>
            <Button onClick={this.handlePause}>Pause</Button>
            <Button onClick={this.handlePause}>Stop</Button>
            <input type="range" name="" id="as" onClick={this.Volumen}/>    
            
          </div>
          <div className="Reproductvideo" align='under'>
            <ReactPlayer
            
            url={'https://youtu.be/A_g3lMcWVy0'}
            controls
            loop
            width='20%'
            height='100%'
            className="react-player"
            />
        </div>
          </div>
    );
  }
}

export default App;
