import React from 'react'
// import playerContext from '../../context/playerContext'

// // Hooks
const fav = () => {
  console.log('I like this one')
}

// Component
function Actions() {
  return (
    <div className="actions">
      <img src="https://www.larazon.es/resizer/eJ4KuCa2pjrqgkTSsJhKdtNICCI=/600x400/smart/filters:format(jpg)/cloudfront-eu-central-1.images.arcpublishing.com/larazon/GE33DP2D35AOHBPCM5CPZ65BAY.jpg" />
      <div className="album_meta">
        <span className="alb_label">ALBUM</span>
        <h1>BIZARRAP</h1>
      </div>
      <div className="action_btns">
        <button onClick={() => fav()} className="fav_btn">
          <i className="far fa-heart fa-2x"></i>
        </button>
        <button onClick={() => fav()} className="fav_btn">
          <i className="far fa-arrow-alt-circle-down fa-2x"></i>
        </button>
        <button onClick={() => fav()} className="fav_btn">
          <i className="fas fa-ellipsis-h fa-2x"></i>
        </button>
      </div>
    </div>
  )
}

export default Actions