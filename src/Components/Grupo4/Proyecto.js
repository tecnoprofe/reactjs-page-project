import React from 'react'
import styled from 'styled-components';

const Link = styled.a`
  align-items: center;
  padding: 5px 10px;
  background: gray;
  color: white;
  font-weight:bold;
  text-decoration:none;
  font-size:25px;
`;


const Proyecto= () => {
    return (  
        <>
        <Contenedor>
            <h3>Lista de Gastos</h3>
            <p>Esta web app nos permitira registrar usuarios para qq luego cada usuario tenga su lista de gastos</p><br></br>
            <p>Estaremos Trabajando con los componentes que nos da firebase Para autenticar usuarios y crear una base de datos, Tambien nos ofrece un hosting Gratuito</p><br></br>
        </Contenedor>
        <Contenedor>
 <Link href="https://proyecto-final-dd40d.web.app/">DEMO</Link> 
       </Contenedor>
       <Contenedor>
        <Link href="https://gitlab.com/alpiretorrezdavicho/proyecto-final-con-react-con-firebase">CODIGO EN GIT LAB</Link>       
       </Contenedor>
       </>
    );
}
 
const Contenedor = styled.div`
     
     padding:10px;
     align-items:center;

    h3{
         color: #fff;
         font-weight:bolder;

    }
    p{
        color: #fff;
         font-weight:bolder;
    }
`;

export default Proyecto;