import React from 'react'
import styled from 'styled-components'
import Integrantes from './Integrantes'
import Blog from './Blog'
import Tienda from './Tienda'
import {NavLink , Switch, Route} from 'react-router-dom'
import Alpire from './Alpire'
import Tictac from './Tictac'
import  Proyecto from './Proyecto/App';

const Ejercio5  = () => {
    return (

        <Contenedor>
         <Menu>
         <NavLink to="/integrantes">INTEGRANTES</NavLink>
         <NavLink to="/reproductor">REPRODUCTOR</NavLink>
         <NavLink to="/tictac">TICTACTOE</NavLink>
         <NavLink to="/proyecto">PROYECTO</NavLink>

         </Menu>


        <main>

        <Switch>
            <Route  exact path="/integrantes"  component={Integrantes} ></Route>
            <Route path="/reproductor" component={Alpire} ></Route>
            <Route path="/tictac" component={Tictac}></Route>
            <Route path="/proyecto" component={Proyecto}></Route>

          </Switch>
        </main>
         

         
        </Contenedor>


      );
}

const Contenedor = styled.div`
    max-width: 1000px;
    padding: 40px;
    width: 90%;
    display: grid;
    gap: 20px;
    grid-template-columns: 2fr 1fr;
    background: #fff;
    margin: 40px 0;
    border-radius: 10px;
    box-shadow: 0px 0px 5px rgba(129, 129, 129, 0.1);
`;
 
const Menu = styled.nav`
    width: 100%;
    text-align: center;
    background: #092c4c;
    grid-column: span 2;
    border-radius: 3px;
 
    a{
        color: #fff;
        display: inline-block;
        padding: 15px 20px;
        text-decoration: none;
        font-weight:bold;
        
    }
 
    a:hover {
        background: #1d85e8;
        text-decoration: none;
    }
`;
 
export default Ejercio5 ;