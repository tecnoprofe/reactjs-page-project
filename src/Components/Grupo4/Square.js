function Square(props) {
    return (

      <center>
        <div className={'square'} {...props}>{props.x ? 'x' : (props.o ? 'o' : '')}</div>

      </center>
      
      );
      
  }
  
  export default Square;