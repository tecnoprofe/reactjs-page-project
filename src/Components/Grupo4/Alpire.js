import React from 'react';
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import styled from 'styled-components';


const Alpire =() => {

  const playlist = [
    { name:"1", src: 'https://hanzluo.s3-us-west-1.amazonaws.com/music/ziyounvshen.mp3' },
    { name:"2",src: 'https://hanzluo.s3-us-west-1.amazonaws.com/music/wuyuwuqing.mp3' },
    { name:"3",src: 'https://hanzluo.s3-us-west-1.amazonaws.com/music/suipian.mp3' },
  ]

  

    const [currentTrack, setTrackIndex] = React.useState(0)
    const handleClickNext = () => {
        console.log('click next')
          setTrackIndex((currentTrack) =>
              currentTrack < playlist.length - 1 ? currentTrack + 1 : 0
          );
      };
    
    const handleEnd = () => {
    
      setTrackIndex((currentTrack) =>
              currentTrack > playlist.length - 1 ? currentTrack +1 : 0
          );
    }
    
    return (
      

          
      <Contenedor >
      <h3>{playlist[currentTrack].name}</h3>
      <AudioPlayer
      
        
        volume="0.5"
        preload='auto'
        src={playlist[currentTrack].src}
        showSkipControls
        onClickNext={handleClickNext}
        onClickPrevious={handleEnd}
        // Try other props!
      />
      
    </Contenedor>
    
  
      );
  }

  const Contenedor = styled.div`

     
    

    h3{
         color: #fff;
         font-weight:bolder;

    }
`;
  
 export default Alpire;