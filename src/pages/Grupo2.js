import React from "react";
import Principal from "../Components/grupo2/Proyecto/Principal";
import TicTacToe from '../Components/grupo2/TicTacToe';
import musica from '../Components/grupo2/Proyecto/musica.jpg';


class Home extends React.Component{        
    render () {
        return (
        <div>
            <div align="left">
                <h1>GRUPO 2</h1>  
                <p>Arias Pestaña Nihurcka <br></br>
                    Arroyo Noe Diego Alexander <br></br>
                    Corrales Sabalo Raidel<br></br>
                    Eguez Mendoza Sebastian<br></br>
                </p> 
            </div>
            <Principal />
           <hr/>
            <TicTacToe />
            
        </div>
        );
    }
}
export default Home;
