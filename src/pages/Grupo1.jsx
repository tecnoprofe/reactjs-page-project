import React from "react";
import Mapa from "../Components/Grupo1/Mapa";
import TetrisGame from "../Components/TetrisGame";
import "../App.css";
import TikTakToe from "../Components/Grupo1/TikTakToe";
import Vibora from "../Components/Grupo1/Snake";

class Home extends React.Component {
  render() {
    return (
      <div>
        <div>
          <hr />
          <h1>GRUPO 1</h1>
          <p>
            Deiby Sejas <br />
            Alexander Sanchez
          </p>
          <Mapa />
          <hr />
          <TetrisGame nombre="Alexander" />
          <hr />
          <Vibora colorVib="#248ec2" colorCom="#1d355e" colorFond="yellow" />
          <TikTakToe />
          <hr />
        </div>
      </div>
    );
  }
}
export default Home;
