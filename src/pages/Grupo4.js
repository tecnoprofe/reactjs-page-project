import React from 'react'
import styled from 'styled-components'
import Integrantes from '../Components/Grupo4/Integrantes'
import {NavLink , Switch, Route} from 'react-router-dom'
import Alpire from '../Components/Grupo4/Alpire'
import Tictac from '../Components/Grupo4/Tictac'
import Proyecto from '../Components/Grupo4/Proyecto'

const Ejercio5  = () => {
    return (

        <center>
        <Contenedor>
         <Menu>
         <NavLink to="/integrantes">INTEGRANTES</NavLink>
         <NavLink to="/reproductor">REPRODUCTOR</NavLink>
         <NavLink to="/tictac">TICTACTOE</NavLink>
         <NavLink to="/proyecto">PROYECTO</NavLink>
         </Menu>


        <main>

        <Switch>
            
            <Route path="/reproductor" component={Alpire}></Route>
            <Route path="/tictac" component={Tictac}></Route>
            <Route path="/proyecto" component={Proyecto}></Route>

            <Route path="/"  component={Integrantes} ></Route>

          </Switch>
        </main>
         

         
        </Contenedor>
        </center>


      );
}

const Contenedor = styled.div`
    max-width: 100%;
    padding: 40px;
    width: 90%;
    display: grid;
    gap: 20px;
    grid-template-columns: 2fr 1fr;
    background: #123;
    margin: 40px 0;
    border-radius: 10px;
    box-shadow: 0px 0px 5px rgba(129, 129, 129, 0.1);
    
`;
 
const Menu = styled.nav`
    width: 100%;
    text-align: center;
    background: #092c4c;
    grid-column: span 2;
    border-radius: 3px;
 
    a {
        color: #fff;
        display: inline-block;
        padding: 15px 20px;
        text-decoration: none;
        
    }
 
    a:hover {
        background: #1d85e8;
        text-decoration: none;
    }
`;
 
export default Ejercio5 ;